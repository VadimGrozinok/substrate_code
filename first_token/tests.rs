fn mint_some_profit_amount(amount: u128) {
    assert_ok!(Token::mint_profit(
        // Origin::ROOT,
        Origin::signed(9),
        amount
    ));

    let token_percentage = default_token_percentage();
    for (_, token_data) in token_percentage {
        let supose_balance: u128 = amount * token_data.percent / 100;
        assert_eq!(Token::balances(token_data.wallet), supose_balance);
    }
}

#[test]
fn test_mint_profit() {
    with_externalities(&mut build_ext(), || {
        add_defaults();
        for (_, res) in default_resources(10_000_000_000_000_u128) {
            call_supply(res.clone());
            call_supply(res.clone());
        }
        assert_eq!(Token::ingredients(ResourceType::Osmium).is_some(), true);
        let ingredients = Token::make_sure_ingredients_enough(10_000_000_000_000_u128);
        assert_eq!(ingredients.is_some(), true);
        assert_eq!(ingredients.unwrap().len(), 9);

        let mint_amount = 10_010_000_000_000_u128;
        mint_some_profit_amount(mint_amount);
        assert_eq!(Token::stampings_count(), 2);
        assert_eq!(Token::stampings(2).minted_amount, 10000000000);
    })
}