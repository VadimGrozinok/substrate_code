decl_module! {
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {
		fn deposit_event() = default;

		pub fn mint_profit(origin, amount: u128) -> Result {
			let _ = ensure_signed(origin)?;
			// ensure_root(origin)?;
			Self::_mint_profit(amount.clone())?;
			Ok(())
		}
	}
}