use super::*;
use crate::models::scalar::DefaultResult;

#[cfg(feature = "std")]
use rstd::collections::btree_map::BTreeMap as Map;

impl<T: Trait> Module<T> {
	pub fn _transfer(from: T::AccountId, to: T::AccountId, amount: u128) -> DefaultResult<()> {
		ensure!(amount >= Self::min_amount_to_transfer(), "Too small amount");
		ensure!(from != to, "The receiver should be different from the sender");

		let mut from_balance: u128 = Self::balances(from.clone());
		ensure!(from_balance >= amount, "Insufficient balance");

		let mut to_balance: u128 = Self::balances(to.clone());

		from_balance -= amount;
		to_balance += amount;

		<Balances<T>>::insert(from.clone(), from_balance);
		<Balances<T>>::insert(to.clone(), to_balance);

		Self::deposit_event(RawEvent::Transfer(from, to, amount));
		Ok(())
	}

	pub fn _burn(who: T::AccountId, amount: u128) -> DefaultResult<()> {
		ensure!(amount >= Self::min_amount_to_burn(), "Too small amount");

		let mut balance: u128 = Self::balances(who.clone());
		ensure!(balance >= amount, "Insufficient balance");

		balance -= amount;
		<Balances<T>>::insert(who.clone(), balance.clone());

		Self::deposit_event(RawEvent::Burn(who, amount));
		Ok(())
	}

	// TODO: Alter solution. Add these fields through tests
	#[cfg(feature = "std")]
	pub fn add_total_supply(amount: u128) {
		TotalSupply::mutate(|x| *x = amount)
	}

	#[cfg(feature = "std")]
	pub fn add_balances(balances: Map<T::AccountId, u128>) {
		for (who, amount) in balances.iter() {
			<Balances<T>>::insert(who, amount);
		}
	}
}
