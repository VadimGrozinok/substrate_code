#![cfg(test)]

use super::*;

use primitives::{Blake2Hasher, H256};
use rstd::collections::btree_map::BTreeMap as Map;
use runtime_io::with_externalities;
use sr_primitives::{
    testing::Header,
    traits::{BlakeTwo256, IdentityLookup},
    weights::Weight,
    Perbill,
};
use support::{assert_noop, assert_ok, impl_outer_origin, parameter_types};
use system::ensure_signed;

impl_outer_origin! {
    pub enum Origin for Test {}
}

// For testing the module, we construct most of a mock runtime. This means
// first constructing a configuration type (`Test`) which `impl`s each of the
// configuration traits of modules we want to use.
#[derive(Clone, Eq, PartialEq)]
pub struct Test;

parameter_types! {
    pub const BlockHashCount: u64 = 250;
    pub const MaximumBlockWeight: Weight = 1024;
    pub const MaximumBlockLength: u32 = 2 * 1024;
    pub const AvailableBlockRatio: Perbill = Perbill::from_percent(75);
}

impl system::Trait for Test {
    type Origin = Origin;
    type Call = ();
    type Index = u64;
    type BlockNumber = u64;
    type Hash = H256;
    type Hashing = BlakeTwo256;
    type AccountId = u64;
    type Lookup = IdentityLookup<Self::AccountId>;
    type Header = Header;
    type WeightMultiplierUpdate = ();
    type Event = ();
    type BlockHashCount = BlockHashCount;
    type MaximumBlockWeight = MaximumBlockWeight;
    type MaximumBlockLength = MaximumBlockLength;
    type AvailableBlockRatio = AvailableBlockRatio;
    type Version = ();
}

impl Trait for Test {
    type Event = ();
    type String = Vec<u8>;
}

type secont_token = Module<Test>;

// This function basically just builds a genesis storage key/value store according to
// our desired mockup.
fn build_ext() -> runtime_io::TestExternalities<Blake2Hasher> {
    system::GenesisConfig::default().build_storage::<Test>().unwrap().into()
}

fn secont_token_total_supply() -> u128 {
    // 3 billions secont_token in fraction(1 = 10_000_000_000)
    30_000_000_000_000_000_000_u128
}

fn add_secont_token_total_supply() {
    assert_eq!(secont_token::total_supply(), 0);
    let d_total_supply = secont_token_total_supply();

    assert_eq!(secont_token::add_total_supply(d_total_supply), ());

    assert_eq!(secont_token::total_supply(), d_total_supply);
}

fn add_secont_token_owner_balance(who: <Test as system::Trait>::AccountId) {
    assert_eq!(secont_token::balances(who.clone()), 0);

    let mut balances = Map::new();
    balances.insert(who, secont_token_total_supply());

    assert_eq!(secont_token::add_balances(balances.clone()), ());
    for (who, balance) in balances.iter() {
        assert_eq!(secont_token::balances(who), *balance);
    }
}

fn add_defaults(who: <Test as system::Trait>::AccountId) {
    add_secont_token_total_supply();
    add_secont_token_owner_balance(who);
}

fn transfer_to_somebody(sender_origin: Origin, receiver_origin: Origin, amount: u128) {
    let sender = ensure_signed(sender_origin.clone()).unwrap();
    let receiver = ensure_signed(receiver_origin.clone()).unwrap();
    let sender_initial_balance = secont_token::balances(sender.clone());
    let receiver_initial_balance = secont_token::balances(receiver.clone());

    assert_ok!(secont_token::transfer(sender_origin.clone(), receiver.clone(), amount));

    assert_eq!(secont_token::balances(sender.clone()), sender_initial_balance - amount);
    assert_eq!(secont_token::balances(receiver.clone()), receiver_initial_balance + amount);
}

#[test]
fn test_transfer_secont_token() {
    with_externalities(&mut build_ext(), || {
        let owner_origin = Origin::signed(0);
        let sender_origin = Origin::signed(1);
        let receiver_origin = Origin::signed(2);

        let amount: u128 = 10_000_000_000;

        add_defaults(0);

        assert_noop!(
            secont_token::transfer(
                sender_origin.clone(),
                ensure_signed(receiver_origin.clone()).unwrap(),
                amount
            ),
            "Insufficient balance"
        );
        transfer_to_somebody(owner_origin.clone(), sender_origin.clone(), amount);
        transfer_to_somebody(sender_origin, receiver_origin, amount);
    })
}

#[test]
fn test_burn_secont_token() {
    with_externalities(&mut build_ext(), || {
        let owner_origin = Origin::signed(0);
        add_defaults(0);

        assert_noop!(
            secont_token::burn(owner_origin.clone(), secont_token_total_supply() + 1),
            "Insufficient balance"
        );
        assert_ok!(secont_token::burn(owner_origin, secont_token_total_supply()));
    })
}
