mod inner;
mod tests;

use rstd::prelude::*;
use support::{decl_event, decl_module, decl_storage, dispatch::Result, ensure, Parameter};
use system::ensure_signed;

pub trait Trait: system::Trait {
	type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>;
	type String: Parameter + From<Vec<u8>> + Into<Vec<u8>> + Default + Clone;
}

decl_event!(
	pub enum Event<T>
	where
		AccountId = <T as system::Trait>::AccountId,
	{
		Transfer(AccountId, AccountId, u128),
		Burn(AccountId, u128),
	}
);

const MIN_AMOUNT_TO_TRANSFER: u128 = 1;
const MIN_AMOUNT_TO_BURN: u128 = 1;

decl_storage! {
	trait Store for Module<T: Trait> as second_token {
		pub MinAmountToTransfer get(min_amount_to_transfer): u128 = MIN_AMOUNT_TO_TRANSFER;
		pub MinAmountToBurn get(min_amount_to_burn): u128 = MIN_AMOUNT_TO_BURN;

		pub TotalSupply get(total_supply) config(): u128;
		pub Balances get(balances) config(): map T::AccountId => u128;
	}
}

decl_module! {
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {
		fn deposit_event() = default;

		pub fn transfer(origin, to: T::AccountId, amount: u128) -> Result {
			let who = ensure_signed(origin)?;
			Self::_transfer(who.clone(), to.clone(), amount.clone())?;
			Ok(())
		}

		pub fn burn(origin, amount: u128) -> Result {
			let who = ensure_signed(origin)?;
			Self::_burn(who.clone(), amount)?;
			Ok(())
		}
	}
}
